package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/adityathebe/assignment/client/routes"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

func init() {
	templates.LoadTemplates()
}

func main() {
	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":9060"
	}
	router := routes.GenerateRoutes()
	if err := http.ListenAndServeTLS(addr, "https-server.crt", "https-server.key", router); err != nil {
		log.Fatal("ListenAndServe: ", err.Error())
	}
}
