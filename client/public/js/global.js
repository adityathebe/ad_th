window.setTimeout(function () {
  const alerts = document.getElementsByClassName('alert');
  for (const alert of alerts) {
    alert.style.display = 'none';
  }
}, 5000);
