package templates

import (
	"html/template"
)

// Templates holds all the loaded templates
var Templates map[string]*template.Template

// LoadTemplates laods all the html files
func LoadTemplates() {
	var baseTemplate = "templates/layout/_base.html"
	Templates = make(map[string]*template.Template)

	Templates["index"] = template.Must(template.ParseFiles(baseTemplate, "templates/home/index.html"))
	Templates["signin"] = template.Must(template.ParseFiles(baseTemplate, "templates/auth/signin.html"))
	Templates["password-reset"] = template.Must(template.ParseFiles(baseTemplate, "templates/auth/password-reset.html"))
	Templates["password-change"] = template.Must(template.ParseFiles(baseTemplate, "templates/auth/password-change.html"))
	Templates["signup"] = template.Must(template.ParseFiles(baseTemplate, "templates/auth/signup.html"))
	Templates["profile"] = template.Must(template.ParseFiles(baseTemplate, "templates/user/profile.html"))
	Templates["edit-profile"] = template.Must(template.ParseFiles(baseTemplate, "templates/user/edit-profile.html"))
}

// TemplateMessage ...
type TemplateMessage struct {
	Error    string
	Success  string
	Token    string
	LoggedIn bool
}

// ProfileEditTemplateMessage ...
type ProfileEditTemplateMessage struct {
	Error         string
	Success       string
	LoggedIn      bool
	FullName      string
	Address       string
	Telephone     string
	Email         string
	EmailDisabled bool
}
