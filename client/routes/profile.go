package routes

import (
	"log"
	"net/http"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// ProfileRoute ...
func ProfileRoute(w http.ResponseWriter, r *http.Request) {
	sessionUser := r.Context().Value("user").(*api.UserProfile)

	if err := templates.Templates["profile"].Execute(w, templates.ProfileEditTemplateMessage{
		FullName:      sessionUser.FullName,
		Telephone:     sessionUser.Telephone,
		Address:       sessionUser.Address,
		LoggedIn:      true,
		Email:         sessionUser.Email,
		EmailDisabled: sessionUser.RegMethod == "google",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// ProfileEditRouteGET returns an html form to edit user profile
func ProfileEditRouteGET(w http.ResponseWriter, r *http.Request) {
	sessionUser := r.Context().Value("user").(*api.UserProfile)

	if err := templates.Templates["edit-profile"].Execute(w, templates.ProfileEditTemplateMessage{
		FullName:      sessionUser.FullName,
		Telephone:     sessionUser.Telephone,
		Address:       sessionUser.Address,
		LoggedIn:      true,
		Email:         sessionUser.Email,
		EmailDisabled: sessionUser.RegMethod == "google",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// ProfileEditRoutePOST updates a user profile
func ProfileEditRoutePOST(w http.ResponseWriter, r *http.Request) {
	token := r.Context().Value("token").(string)
	sessionUser := r.Context().Value("user").(*api.UserProfile)

	if err := r.ParseForm(); err != nil {
		if err := templates.Templates["/edit_profile"].Execute(w, templates.TemplateMessage{
			Error: "Malformed Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	updatedProfile := api.UserProfileUpdate{
		Email:     r.PostForm.Get("email"),
		Telephone: r.PostForm.Get("telephone"),
		Address:   r.PostForm.Get("address"),
		FullName:  r.PostForm.Get("fullname"),
	}

	apiResp, err := api.UpdateUserProfile(token, updatedProfile)
	if err != nil {
		log.Println(err.Error())
		if err := r.ParseForm(); err != nil {
			if err := templates.Templates["edit-profile"].Execute(w, templates.ProfileEditTemplateMessage{
				Error:         "Something went wrong",
				FullName:      sessionUser.FullName,
				Telephone:     sessionUser.Telephone,
				Address:       sessionUser.Address,
				LoggedIn:      true,
				Email:         sessionUser.Email,
				EmailDisabled: sessionUser.RegMethod == "google",
			}); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			return
		}
	}

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["edit-profile"].Execute(w, templates.ProfileEditTemplateMessage{
			Error:         apiResp.Err,
			FullName:      sessionUser.FullName,
			Telephone:     sessionUser.Telephone,
			Address:       sessionUser.Address,
			Email:         sessionUser.Email,
			LoggedIn:      true,
			EmailDisabled: sessionUser.RegMethod == "google",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	user, err := api.GetUserProfile(token)
	if err != nil {
		log.Println(err.Error())
		if err := r.ParseForm(); err != nil {
			if err := templates.Templates["edit-profile"].Execute(w, templates.ProfileEditTemplateMessage{
				Error:         "Something went wrong",
				FullName:      sessionUser.FullName,
				Telephone:     sessionUser.Telephone,
				Address:       sessionUser.Address,
				Email:         sessionUser.Email,
				LoggedIn:      true,
				EmailDisabled: sessionUser.RegMethod == "google",
			}); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			return
		}
	}
	if err := templates.Templates["profile"].Execute(w, templates.ProfileEditTemplateMessage{
		FullName:      user.FullName,
		Telephone:     user.Telephone,
		Address:       user.Address,
		Email:         user.Email,
		EmailDisabled: user.RegMethod == "google",
		LoggedIn:      true,
		Success:       "Success",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
