package routes

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// SignupRouteGET returns the sign up page
func SignupRouteGET(w http.ResponseWriter, r *http.Request) {
	if err := templates.Templates["signup"].Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// SignupRoutePOST Registers a new User
func SignupRoutePOST(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		if err := templates.Templates["signup"].Execute(w, templates.TemplateMessage{
			Error: "Malformed Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	email := r.PostForm.Get("email")
	password := r.PostForm.Get("password")
	postData := fmt.Sprintf(`{"email":"%s","password":"%s"}`, email, password)
	apiResp, err := api.CallAPI("/register", []byte(postData))

	if err != nil {
		if err := templates.Templates["signup"].Execute(w, templates.TemplateMessage{
			Error: "Internal Server Errror",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["signup"].Execute(w, templates.TemplateMessage{
			Error: apiResp.Err,
		}); err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
		}
		return
	}

	// Set Cookie
	c := &http.Cookie{
		Name:     "auth",
		Value:    apiResp.Token,
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Now().Add(time.Hour),
		SameSite: http.SameSiteLaxMode,
	}
	http.SetCookie(w, c)
	http.Redirect(w, r, "/edit_profile", http.StatusFound)
}
