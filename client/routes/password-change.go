package routes

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// PasswordChangeRouteGET ...
func PasswordChangeRouteGET(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get("token")
	if token == "" {
		if err := templates.Templates["password-change"].Execute(w, templates.TemplateMessage{
			Error: "Broken Link",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if err := templates.Templates["password-change"].Execute(w, templates.TemplateMessage{
		Token: token,
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// PasswordChangeRoutePOST ...
func PasswordChangeRoutePOST(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		if err := templates.Templates["password-change"].Execute(w, templates.TemplateMessage{
			Error: "Malformed Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	token := r.PostForm.Get("token")
	password := r.PostForm.Get("password")
	postData := fmt.Sprintf(`{"token":"%s","password":"%s"}`, token, password)
	apiResp, err := api.CallAPI("/reset-password-consume", []byte(postData))
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["password-change"].Execute(w, templates.TemplateMessage{
			Error: apiResp.Err,
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
		Success: "Password Changed Successfully",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	return
}
