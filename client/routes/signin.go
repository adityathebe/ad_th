package routes

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// SigninRouteGET returns in the sign in page
func SigninRouteGET(w http.ResponseWriter, r *http.Request) {
	if err := templates.Templates["signin"].Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// SigninRoutePOST authenticates a user
func SigninRoutePOST(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
			Error: "Malformed Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	// Call REST API for authentication
	email := r.PostForm.Get("email")
	password := r.PostForm.Get("password")
	postData := fmt.Sprintf(`{"email":"%s","password":"%s"}`, email, password)
	apiResp, err := api.CallAPI("/login", []byte(postData))

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
			Error: apiResp.Err,
		}); err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
		}
		return
	}

	if err != nil {
		if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
			Error: "Internal Server Errror",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// Set Cookie
	c := &http.Cookie{
		Name:     "auth",
		Value:    apiResp.Token,
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Now().Add(time.Hour),
		SameSite: http.SameSiteLaxMode,
	}
	http.SetCookie(w, c)
	http.Redirect(w, r, "/profile", http.StatusFound)
}
