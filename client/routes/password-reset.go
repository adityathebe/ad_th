package routes

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// PasswordResetRouteGET ...
func PasswordResetRouteGET(w http.ResponseWriter, r *http.Request) {
	if err := templates.Templates["password-reset"].Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// PasswordResetRoutePOST ...
func PasswordResetRoutePOST(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		if err := templates.Templates["password-reset"].Execute(w, templates.TemplateMessage{
			Error: "Malformed Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	email := r.PostForm.Get("email")

	apiPath := fmt.Sprintf("/reset-password?email=%s&client_url=%s/password_change?token=", email, os.Getenv("PUBLIC_URL"))
	apiResp, err := api.CallAPIGET(apiPath)

	if err != nil {
		if err := templates.Templates["password-reset"].Execute(w, templates.TemplateMessage{
			Error: "Internal Server Error",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["password-reset"].Execute(w, templates.TemplateMessage{
			Error: apiResp.Err,
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if err := templates.Templates["password-reset"].Execute(w, templates.TemplateMessage{
		Success: "An email with the link to reset your password has been sent",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
