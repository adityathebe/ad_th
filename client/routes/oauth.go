package routes

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// HandleConsumeOauthCode will handle the Google oauth authorization code
func HandleConsumeOauthCode(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	authCode := query.Get("code")
	if authCode == "" {
		if err := templates.Templates["index"].Execute(w, templates.TemplateMessage{
			Error: "Bad Oauth Request",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// Call API
	endpoint := fmt.Sprintf("/oauth/consume?redirect_uri=%s&%s", os.Getenv("OAUTH_REDIRECT_URL"), r.URL.Query().Encode())
	apiResp, err := api.CallAPIGET(endpoint)
	if err != nil {
		if err := templates.Templates["index"].Execute(w, templates.TemplateMessage{
			Error: "Something went wrong",
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	if apiResp.StatusCode != 200 {
		if err := templates.Templates["index"].Execute(w, templates.TemplateMessage{
			Error: apiResp.Err,
		}); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	// Set Cookie
	cookie := &http.Cookie{
		Name:     "auth",
		Value:    apiResp.Token,
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Now().Add(time.Hour),
		SameSite: http.SameSiteLaxMode,
	}
	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
}

// HandleGoogleOauthInit Redirects to google oauth
func HandleGoogleOauthInit(w http.ResponseWriter, r *http.Request) {
	clientID := os.Getenv("OAUTH_CLIENT_ID")
	scope := "openid profile email"
	redirectURI := os.Getenv("OAUTH_REDIRECT_URL")
	url := fmt.Sprintf("https://accounts.google.com/o/oauth2/v2/auth?client_id=%s&response_type=code&scope=%s&redirect_uri=%s",
		clientID,
		scope,
		redirectURI)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}
