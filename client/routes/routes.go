package routes

import (
	"net/http"

	"bitbucket.org/adityathebe/assignment/client/middlewares"
	"github.com/gorilla/mux"
)

// GenerateRoutes creates a new mux router
func GenerateRoutes() *mux.Router {
	router := mux.NewRouter()

	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("public/"))))

	router.HandleFunc("/", IndexRoute).Methods("GET")
	router.HandleFunc("/signout", SignOutHandler).Methods("GET")

	// Sign in
	router.HandleFunc("/signin", SigninRouteGET).Methods("GET")
	router.HandleFunc("/signin", SigninRoutePOST).Methods("POST")

	// Sign up
	router.HandleFunc("/signup", SignupRouteGET).Methods("GET")
	router.HandleFunc("/signup", SignupRoutePOST).Methods("POST")

	// Password Change functionality
	router.HandleFunc("/password_reset", PasswordResetRouteGET).Methods("GET")
	router.HandleFunc("/password_reset", PasswordResetRoutePOST).Methods("POST")
	router.HandleFunc("/password_change", PasswordChangeRouteGET).Methods("GET")
	router.HandleFunc("/password_change", PasswordChangeRoutePOST).Methods("POST")

	// OAUTH
	router.HandleFunc("/oauth/consume", HandleConsumeOauthCode).Methods("GET")
	router.HandleFunc("/oauth/google", HandleGoogleOauthInit).Methods("GET")

	// Profile
	router.HandleFunc("/profile", middlewares.JWTMiddleware(ProfileRoute)).Methods("GET")
	router.HandleFunc("/edit_profile", middlewares.JWTMiddleware(ProfileEditRouteGET)).Methods("GET")
	router.HandleFunc("/edit_profile", middlewares.JWTMiddleware(ProfileEditRoutePOST)).Methods("POST")
	return router
}
