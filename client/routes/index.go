package routes

import (
	"net/http"
	"time"

	"bitbucket.org/adityathebe/assignment/client/templates"
)

// IndexRoute serves the home page
func IndexRoute(w http.ResponseWriter, r *http.Request) {
	if err := templates.Templates["signin"].Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// SignOutHandler clears the user cookie in the client side
func SignOutHandler(w http.ResponseWriter, r *http.Request) {
	c := &http.Cookie{
		Name:     "auth",
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}
	http.SetCookie(w, c)
	if err := templates.Templates["signin"].Execute(w, nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
