package middlewares

import (
	"context"
	"net/http"

	"bitbucket.org/adityathebe/assignment/client/api"
	"bitbucket.org/adityathebe/assignment/client/templates"
)

// JWTMiddleware ensures that all request are authenticated
func JWTMiddleware(handler http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookies := r.Cookies()
		token := ""
		for _, cookie := range cookies {
			if cookie.Name == "auth" {
				token = cookie.Value
			}
		}

		if token == "" {
			if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
				Error: "Please Sign in to continue.",
			}); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			return
		}

		user, err := api.GetUserProfile(token)
		if err != nil {
			if err := templates.Templates["signin"].Execute(w, templates.TemplateMessage{
				Error: "Something went wrong.",
			}); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
		ctx := context.WithValue(r.Context(), "user", user)
		ctx = context.WithValue(ctx, "token", token)

		w.Header().Set("Cache-Control", "private, nocache, no-store, max-age=0, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "Fri, 01 Jan 1990 00:00:00 GMT")
		handler.ServeHTTP(w, r.WithContext(ctx))
	})
}
