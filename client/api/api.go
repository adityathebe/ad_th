package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
)

// CallAPI communicates with the REST API
func CallAPI(path string, jsonData []byte) (*GenericResponse, error) {
	url := os.Getenv("API_URL") + path
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var apiResp GenericResponse
	apiResp.StatusCode = resp.StatusCode
	err = json.NewDecoder(resp.Body).Decode(&apiResp)
	if err != nil {
		return nil, err
	}
	return &apiResp, nil
}

// CallAPIGET communicates with the REST API
func CallAPIGET(path string) (*GenericResponse, error) {
	url := os.Getenv("API_URL") + path
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var apiResp GenericResponse
	apiResp.StatusCode = resp.StatusCode
	err = json.NewDecoder(resp.Body).Decode(&apiResp)
	if err != nil {
		return nil, err
	}
	return &apiResp, nil
}

// GetUserProfile communicates with the REST API with Access Token
func GetUserProfile(token string) (*UserProfile, error) {
	url := os.Getenv("API_URL") + "/user/profile"
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var apiResp UserProfile
	err = json.NewDecoder(resp.Body).Decode(&apiResp)
	if err != nil {
		return nil, err
	}
	return &apiResp, nil
}

// UpdateUserProfile ...
func UpdateUserProfile(token string, data UserProfileUpdate) (*GenericResponse, error) {
	url := os.Getenv("API_URL") + "/user/profile"

	b, _ := json.Marshal(data)
	req, err := http.NewRequest("PATCH", url, bytes.NewBuffer(b))
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var apiResp GenericResponse
	apiResp.StatusCode = resp.StatusCode
	err = json.NewDecoder(resp.Body).Decode(&apiResp)
	if err != nil {
		return nil, err
	}
	return &apiResp, nil
}
