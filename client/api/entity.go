package api

import (
	"time"
)

// GenericResponse is the response from the REST API
type GenericResponse struct {
	Success    string `json:"success"`
	Err        string `json:"error"`
	Token      string `json:"token"`
	StatusCode int
}

// UserProfile is the response from the REST API
type UserProfile struct {
	ID                int       `json:"id"`
	FullName          string    `json:"full_name"`
	Address           string    `json:"address"`
	Telephone         string    `json:"telephone"`
	Email             string    `json:"email"`
	RegMethod         string    `json:"reg_method"`
	ResetToken        string    `json:"reset_token"`
	ResetTokenExpTime time.Time `json:"reset_token_exp_time"`
}

// UserProfileUpdate ...
type UserProfileUpdate struct {
	FullName  string `json:"full_name"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Email     string `json:"email"`
}
