package main

import (
	"log"
	"os"

	"bitbucket.org/adityathebe/assignment/server/api"
	"bitbucket.org/adityathebe/assignment/server/api/database"
)

func main() {
	db := database.NewDB(os.Getenv("MYSQL_CONNECTION_STRING"))
	db.Open()

	server := api.NewServer()
	server.Addr = os.Getenv("ADDR")
	server.UserService = database.NewUserService(db)
	log.Fatal(server.Open())
}
