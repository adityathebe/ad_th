package utils

import (
	"crypto/rand"
	"fmt"
)

// PseudoUUID - NOT RFC4122 compliant
// https://stackoverflow.com/a/25736155/6199444
func PseudoUUID() (uuid string) {
	b := make([]byte, 16)
	rand.Read(b)
	uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return uuid
}
