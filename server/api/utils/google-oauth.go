package utils

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
)

// GoogleAuthResponse is the token exchange response from Google
type GoogleAuthResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	IDToken     string `json:"id_token"`
	Error       string `json:"error"`
}

// GoogleUserInfo is the openid user info
type GoogleUserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Locale        string `json:"locale"`
}

// GoogleAPIError is the error json response from google auth server
type GoogleAPIError struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

// ExchangeCodeForToken will exchange google oauth code for an access token
func ExchangeCodeForToken(code, clientRedirectURI string) (token string, err error) {
	req, err := http.NewRequest("POST", "https://accounts.google.com/o/oauth2/token", nil)
	if err != nil {
		return "", err
	}

	q := req.URL.Query()
	q.Add("client_id", os.Getenv("GOOGLE_CLIENT_ID"))
	q.Add("client_secret", os.Getenv("GOOGLE_CLIENT_SECRET"))
	q.Add("grant_type", "authorization_code")
	q.Add("redirect_uri", clientRedirectURI)
	q.Add("code", code)
	req.URL.RawQuery = q.Encode()

	// Call Google API
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	var oauthResp GoogleAuthResponse
	err = json.NewDecoder(resp.Body).Decode(&oauthResp)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "", errors.New(oauthResp.Error)
	}

	return oauthResp.AccessToken, nil
}

// GetGoogleUserInfo ...
func GetGoogleUserInfo(token string) (*GoogleUserInfo, error) {
	url := "https://www.googleapis.com/oauth2/v3/userinfo"

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, nil
	}

	if resp.StatusCode != 200 {
		var apiError GoogleAPIError
		err = json.NewDecoder(resp.Body).Decode(&apiError)
		if err != nil {
			return nil, err
		}
		return nil, errors.New(apiError.Error)
	}

	var userInfo GoogleUserInfo
	err = json.NewDecoder(resp.Body).Decode(&userInfo)
	if err != nil {
		return nil, err
	}

	return &userInfo, nil
}
