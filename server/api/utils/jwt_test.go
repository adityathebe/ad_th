package utils

import (
	"testing"
)

func TestJWT(t *testing.T) {
	secret := "adityathebe"
	payload := JWTPayload{
		Email: "john@doe.com",
		ID:    1,
	}
	got, err := GenerateJWT(payload, secret)
	if err != nil {
		t.Fatal()
	}

	want := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG5AZG9lLmNvbSIsImlkIjoxfQ._qTmhd456odUAR7ZOkFjEBCzoQo65jIPBW6p9LMG3KQ"
	if got != want {
		t.Errorf("Got %s, want %s", got, want)
	}
}

func TestUUIDLength(t *testing.T) {
	got := PseudoUUID()
	if len(got) != 36 {
		t.Errorf("Got %d, want %d", len(got), 36)
	}
}
