package utils

import (
	"bytes"
	"fmt"
	"html/template"
	"net/smtp"
	"os"
)

const smtpHost = "smtp.gmail.com"
const smtpPort = "587"
const mimeHeaders = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"

const passResetTemplate = `
<!DOCTYPE html>
<html>
<body>
  <a href="{{.ResetLink}}">Click here to reset your password Token</a>
</body>
</html>`

// SendPasswordResetEmail emails the password reset link
func SendPasswordResetEmail(email, clientPasswordResetLink string) error {
	from := os.Getenv("SUPPORT_EMAIL_ID")
	password := os.Getenv("SUPPORT_EMAIL_PASS")
	receiverEmails := []string{email}

	var body bytes.Buffer
	body.Write([]byte(fmt.Sprintf("Subject: Password Reset\n%s\n\n", mimeHeaders)))
	tmpl, _ := template.New("email").Parse(passResetTemplate)
	tmpl.Execute(&body, struct {
		ResetLink string
	}{
		ResetLink: clientPasswordResetLink,
	})

	auth := smtp.PlainAuth("", from, password, smtpHost)
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, receiverEmails, body.Bytes())
	if err != nil {
		return err
	}
	return nil
}
