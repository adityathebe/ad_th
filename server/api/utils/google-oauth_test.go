package utils

import (
	"testing"
)

func TestExchangeCodeForToken(t *testing.T) {
	_, err := GetGoogleUserInfo("BAD_CODE")
	if err == nil {
		t.Error("Should return an error")
	}
	if err.Error() != "invalid_request" {
		t.Error("Should return 'invalid_request' error")
	}
}
