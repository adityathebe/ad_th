package utils

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
)

// JWT is JSON Web Token
type JWT struct {
	Header    string
	Payload   string
	Signature string
}

// JWTPayload represents the JSON JWT payload
type JWTPayload struct {
	Email string `json:"email"`
	ID    int    `json:"id"`
}

const jwtHeader = `{"alg":"HS256","typ":"JWT"}`

// ParseJWT verifies if the jwt is in valid format
func ParseJWT(jwt string) (JWT, error) {
	x := strings.Split(jwt, ".")
	if len(x) != 3 {
		return JWT{}, errors.New("Invalid JWT. Not enough or too many segments")
	}
	return JWT{
		Header:    x[0],
		Payload:   x[1],
		Signature: x[2],
	}, nil
}

// HS256 returns the HMAC-SHA256
func HS256(data, secret string) string {
	mac := hmac.New(sha256.New, []byte(secret))
	mac.Write([]byte(data))
	return base64.RawURLEncoding.EncodeToString(mac.Sum(nil))
}

// GenerateJWT generates base64 encoded JWT
func GenerateJWT(payload JWTPayload, secret string) (string, error) {
	header := base64.RawURLEncoding.EncodeToString([]byte(jwtHeader))

	b, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}
	payloadB64 := base64.RawURLEncoding.EncodeToString(b)
	toSign := header + "." + payloadB64
	sign := HS256(toSign, secret)
	return header + "." +
		payloadB64 + "." +
		sign, nil
}

// GetJWTPayload parses the JWT payload
// and returns the associated email
func GetJWTPayload(payload string) (*JWTPayload, error) {
	data, err := base64.RawURLEncoding.DecodeString(payload)
	if err != nil {
		return nil, err
	}

	var jwtPayload JWTPayload
	json.Unmarshal(data, &jwtPayload)
	return &jwtPayload, nil
}
