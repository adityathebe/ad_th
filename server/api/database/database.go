package database

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

// DB represents the database connection.
type DB struct {
	DB         *sql.DB
	ConnString string
}

// NewDB returns a new instance of DB associated with the given datasource name.
func NewDB(connString string) *DB {
	db := &DB{
		ConnString: connString,
	}
	return db
}

// Open opens the database connection.
func (db *DB) Open() (err error) {
	database, err := sql.Open("mysql", db.ConnString)
	if err != nil {
		return err
	}
	db.DB = database
	return
}

// Close closes the database connection.
func (db *DB) Close() error {
	// Close database.
	if db.DB != nil {
		return db.DB.Close()
	}
	return nil
}
