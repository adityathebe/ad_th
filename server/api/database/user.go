package database

import (
	"database/sql"
	"errors"
	"strings"
	"time"

	"bitbucket.org/adityathebe/assignment/server/api/entity"
	"golang.org/x/crypto/bcrypt"
)

// Ensure service implements interface.
var _ entity.UserService = (*UserService)(nil)

// UserService represents a service for managing users
type UserService struct {
	db *DB
}

// NewUserService returns a new instance of UserService.
func NewUserService(db *DB) *UserService {
	return &UserService{db: db}
}

// FindUserByID retrieves a user by ID.
func (s *UserService) FindUserByID(id int) (*entity.User, error) {
	var user entity.User

	// Execute the query
	row := s.db.DB.QueryRow(`SELECT id, COALESCE(full_name, ''), COALESCE(address, ''), COALESCE(telephone, ''), 
		COALESCE(password, ''),	email, COALESCE(reg_method, '') from users WHERE id = ?`, id)
	err := row.Scan(&user.ID, &user.FullName, &user.Address, &user.Telephone, &user.Password, &user.Email, &user.RegMethod)
	if err != nil {
		return &user, err
	}
	return &user, nil
}

// FindUserByEmail retrieves user by email.
func (s *UserService) FindUserByEmail(email string) (*entity.User, error) {
	var user entity.User

	// Execute the query
	row := s.db.DB.QueryRow(`SELECT id, COALESCE(full_name, ''), COALESCE(address, ''), COALESCE(telephone, ''), 
		COALESCE(password, ''),	email, COALESCE(reg_method, '') from users WHERE email = ?`, email)
	err := row.Scan(&user.ID, &user.FullName, &user.Address, &user.Telephone, &user.Password, &user.Email, &user.RegMethod)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// CreateUser creates a new user.
func (s *UserService) CreateUser(user entity.User) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	// Save new user
	sqlStatement := "INSERT INTO users (email, password, reg_method) VALUES (?, ?, ?)"
	_, err = s.db.DB.Exec(sqlStatement, user.Email, hashedPassword, "local")

	if err != nil {
		if strings.Contains(err.Error(), "Duplicate entry") {
			return errors.New("User with the email already exists")
		}
		return err
	}
	return err
}

// CreateGoogleUser creates a new user with google oauth.
func (s *UserService) CreateGoogleUser(user entity.User) error {
	sqlStatement := "INSERT INTO users (email, full_name, google_id, reg_method) VALUES (?,?, ?, ?)"
	_, err := s.db.DB.Exec(sqlStatement, user.Email, user.FullName, user.GoogleID, "google")

	if err != nil {
		if strings.Contains(err.Error(), "Duplicate entry") {
			return nil
		}
		return err
	}

	return nil
}

// UpdateResetToken updates the password reset token of an existing user
func (s *UserService) UpdateResetToken(userID int, token string, expTime time.Time) error {

	sqlStatement := "UPDATE users SET reset_token = ?, reset_token_exp_time = ? WHERE id = ?"
	_, err := s.db.DB.Exec(sqlStatement, token, expTime, userID)

	if err != nil {
		return err
	}

	return nil
}

// FindUserByResetToken retrieves a user by reset token
func (s *UserService) FindUserByResetToken(token string) (*entity.User, error) {
	var user entity.User

	// Execute the query
	row := s.db.DB.QueryRow("SELECT id, email, COALESCE(password, ''), reset_token_exp_time from users WHERE reset_token = ?", token)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.ResetTokenExpTime)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// UpdateUserPassword updates the user password
func (s *UserService) UpdateUserPassword(userID int, password string) error {
	// hash password
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	_, err = s.db.DB.Exec("UPDATE users SET password = ? WHERE id = ?", hashedPassword, userID)
	if err != nil {
		return err
	}

	return nil
}

// ClearResetToken clears the reset token and the
// token expiry date
func (s *UserService) ClearResetToken(userID int) error {
	_, err := s.db.DB.Exec("UPDATE users SET reset_token = NULL, reset_token_exp_time = NULL")
	if err != nil {
		return err
	}

	return nil
}

// UpdateUser updates user profile
func (s *UserService) UpdateUser(id int, userUpdate entity.UserUpdate) error {
	user, err := s.FindUserByID(id)
	if err != nil {
		return err
	}

	if email := userUpdate.Email; user.RegMethod == "local" && email != "" {
		emailUser, err := s.FindUserByEmail(email)
		if err != nil {
			if err == sql.ErrNoRows {
				user.Email = email
			} else {
				return err
			}
		} else {
			if emailUser.ID != user.ID {
				return errors.New("Another user with that email is already registered")
			}
		}
	}

	if address := userUpdate.Address; address != "" {
		user.Address = address
	}

	if telephone := userUpdate.Telephone; telephone != "" {
		user.Telephone = telephone
	}

	if fullname := userUpdate.FullName; fullname != "" {
		user.FullName = fullname
	}

	if _, err := s.db.DB.Exec("UPDATE users SET email = ?, address =?, telephone =?, full_name =? WHERE id = ?",
		user.Email,
		user.Address,
		user.Telephone,
		user.FullName,
		user.ID); err != nil {
		return err
	}

	return nil
}
