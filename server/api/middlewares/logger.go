package middlewares

import (
	"log"
	"net/http"
)

// LoggingMiddleware logs the incoming HTTP request
func LoggingMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s - %s %s %s", r.RemoteAddr, r.Proto, r.Method, r.URL.RequestURI())
		h.ServeHTTP(w, r)
	})
}
