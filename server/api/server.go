package api

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/adityathebe/assignment/server/api/database"
	"bitbucket.org/adityathebe/assignment/server/api/middlewares"
	"bitbucket.org/adityathebe/assignment/server/api/utils"
	"github.com/gorilla/mux"
)

// Server represents an HTTP server.
type Server struct {
	server *http.Server
	router *mux.Router

	Addr string

	// Google OAuth settings.
	GoogleClientID     string
	GoogleClientSecret string

	JWTSecret string

	// Servics used by the various HTTP routes.
	UserService *database.UserService
}

// NewServer returns a new instance of Server.
func NewServer() *Server {
	// Create a new server that wraps the net/http server & add a gorilla router.
	s := &Server{
		server: &http.Server{},
		router: mux.NewRouter(),
	}
	return s
}

// Open validates the server options and begins listening on the bind address.
func (s *Server) Open() (err error) {
	// Validate Google OAuth settings.
	s.GoogleClientID = os.Getenv("GOOGLE_CLIENT_ID")
	s.GoogleClientSecret = os.Getenv("GOOGLE_CLIENT_SECRET")
	s.JWTSecret = os.Getenv("JWT_SECRET")
	if s.GoogleClientID == "" {
		log.Fatal("GOOGLE_CLIENT_ID required")
	} else if s.GoogleClientSecret == "" {
		log.Fatal("GOOGLE_CLIENT_SECRET required")
	} else if s.JWTSecret == "" {
		log.Fatal("JWT_SECRET required")
	}

	r := mux.NewRouter()
	s.RegisterAuthRoutes(r)
	s.RegisterUserRoutes(r)

	s.server.Handler = middlewares.LoggingMiddleware(r)
	s.server.Addr = s.Addr

	return s.server.ListenAndServe()
}

// JWTMiddleware ensures that all request are authenticated
func (s *Server) JWTMiddleware(handler http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqToken := r.Header.Get("Authorization")
		splitToken := strings.Split(reqToken, "Bearer ")

		if len(splitToken) != 2 {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{ "error": "%v"}`, "Access Token Required"), http.StatusUnauthorized)
			return
		}

		reqToken = splitToken[1]
		jwt, err := utils.ParseJWT(reqToken)
		if err != nil {
			http.Error(w, `{"error": "Invalid JWT"}`, http.StatusBadRequest)
			return
		}

		sign := utils.HS256(jwt.Header+"."+jwt.Payload, os.Getenv("JWT_SECRET"))
		if sign != jwt.Signature {
			http.Error(w, `{"error": "Bad Signature"}`, http.StatusBadRequest)
			return
		}

		// Parse JWT Payload
		jwtPayload, err := utils.GetJWTPayload(jwt.Payload)
		if err != nil {
			log.Println(err)
			http.Error(w, `{"error": "Error parsing jwt payload"}`, http.StatusInternalServerError)
			return
		}

		user, err := s.UserService.FindUserByID(jwtPayload.ID)
		if err != nil {
			http.Error(w, `{"error": "Bad Authentication"}`, http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "user", user)
		handler.ServeHTTP(w, r.WithContext(ctx))
	})
}
