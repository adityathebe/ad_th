package api

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/adityathebe/assignment/server/api/entity"
	"bitbucket.org/adityathebe/assignment/server/api/utils"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

// RegisterAuthRoutes is a helper function for registering all Auth routes.
func (s *Server) RegisterAuthRoutes(r *mux.Router) {
	r.HandleFunc("/login", s.HandleUserLoginLocal).Methods("POST")
	r.HandleFunc("/register", s.HandleUserRegisterationLocal).Methods("POST")
	r.HandleFunc("/reset-password", s.HandleResetPasswordTokenRequest).Methods("GET")
	r.HandleFunc("/reset-password-consume", s.HandleResetPassword).Methods("POST")
	r.HandleFunc("/oauth/consume", s.HandleConsumeOauthCode).Methods("GET")
}

// HandleUserLoginLocal signs in a user with an email and a password
func (s *Server) HandleUserLoginLocal(w http.ResponseWriter, r *http.Request) {
	var user entity.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if user.Email == "" {
		respMsg := `{"error": "No Email address provided"}`
		http.Error(w, respMsg, http.StatusBadRequest)
		return
	}

	if user.Password == "" {
		respMsg := `{"error": "No Password provided"}`
		http.Error(w, respMsg, http.StatusBadRequest)
		return
	}

	dbUser, err := s.UserService.FindUserByEmail(user.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			respMsg := `{"error": "User Not Found"}`
			http.Error(w, respMsg, http.StatusBadRequest)
			return
		}
		respMsg := fmt.Sprintf(`{"error": "%v"}`, err.Error())
		http.Error(w, respMsg, http.StatusInternalServerError)
		return
	}

	if dbUser.RegMethod != "local" {
		respMsg := `{"error": "User registered with Google"}`
		http.Error(w, respMsg, http.StatusBadRequest)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(user.Password))
	if err != nil {
		respMsg := `{"error": "Bad Authentication"}`
		http.Error(w, respMsg, http.StatusUnauthorized)
		return
	}

	jwt, err := utils.GenerateJWT(utils.JWTPayload{
		Email: dbUser.Email,
		ID:    dbUser.ID,
	}, s.JWTSecret)
	if err != nil {
		respMsg := fmt.Sprintf(`{"error": "%s"}`, err.Error())
		http.Error(w, respMsg, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	response := fmt.Sprintf(`{ "status": "success", "token": %q}`, jwt)
	fmt.Fprint(w, response)
}

// HandleUserRegisterationLocal registers a new user
func (s *Server) HandleUserRegisterationLocal(w http.ResponseWriter, r *http.Request) {
	var user entity.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if user.Email == "" {
		http.Error(w, `{"error": "No Email address provided"}`, http.StatusBadRequest)
		return
	}

	if user.Password == "" {
		http.Error(w, `{"error": "No password provided"}`, http.StatusBadRequest)
		return
	}

	err = s.UserService.CreateUser(user)
	if err != nil {
		log.Println(err)
		errMsg := fmt.Sprintf(`{"error": "%v"}`, err.Error())
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	dbUser, err := s.UserService.FindUserByEmail(user.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jwt, err := utils.GenerateJWT(utils.JWTPayload{
		Email: dbUser.Email,
		ID:    dbUser.ID,
	}, s.JWTSecret)

	fmt.Fprintf(w, `{ "status": "success", "token": %q}`, jwt)
}

// HandleResetPasswordTokenRequest sends a password reset email
func (s *Server) HandleResetPasswordTokenRequest(w http.ResponseWriter, r *http.Request) {
	// Get The email
	email := r.URL.Query().Get("email")
	clientURL := r.URL.Query().Get("client_url")
	if email == "" || clientURL == "" {
		http.Error(w, `{"error":"email or client_url not provided"}`, http.StatusBadRequest)
		return
	}

	// Check if the email exists
	user, err := s.UserService.FindUserByEmail(email)
	if err != nil {
		if err == sql.ErrNoRows {
			respMsg := `{"error": "User Not Found"}`
			http.Error(w, respMsg, http.StatusBadRequest)
			return
		}
		log.Println(err.Error())
		http.Error(w, `{"error":"Something went wrong"}`, http.StatusInternalServerError)
		return
	}

	if user == nil {
		http.Error(w, `{"error":"User with that email doesn't exist"}`, http.StatusBadRequest)
		return
	}

	if user.RegMethod != "local" {
		http.Error(w, `{"error":"User signed up with Google"}`, http.StatusBadRequest)
		return
	}

	// Create the reset token
	token := utils.PseudoUUID()
	expTime := time.Now().Add(time.Minute * 15)
	if err := s.UserService.UpdateResetToken(user.ID, token, expTime); err != nil {
		log.Println(err.Error())
		http.Error(w, `{"error":"Something went wrong"}`, http.StatusInternalServerError)
		return
	}

	// Send Email
	if err := utils.SendPasswordResetEmail(email, clientURL+string(token)); err != nil {
		log.Println(err.Error())
		http.Error(w, `{"error":"Error sending email"}`, http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, `{"success":"Password reset email sent"}`)
}

// HandleResetPassword updates the users's password
// if a valid reset token is supplied
func (s *Server) HandleResetPassword(w http.ResponseWriter, r *http.Request) {
	var resetTokenMsg ResetTokenMessage
	err := json.NewDecoder(r.Body).Decode(&resetTokenMsg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if resetTokenMsg.Token == "" {
		http.Error(w, `{"error":"Token not provided"}`, http.StatusBadRequest)
		return
	}

	// Check if the email exists
	user, err := s.UserService.FindUserByResetToken(resetTokenMsg.Token)
	if err != nil {
		if err == sql.ErrNoRows {
			http.Error(w, `{"error":"Reset Token has expired"}`, http.StatusInternalServerError)
			return
		}
		log.Println(err.Error())
		http.Error(w, `{"error":"Something went wrong"}`, http.StatusInternalServerError)
		return
	}

	// Check if expired
	now := time.Now()
	if now.After(user.ResetTokenExpTime) {
		http.Error(w, `{"error":"Reset Token has expired"}`, http.StatusBadRequest)
		s.UserService.ClearResetToken(user.ID)
		return
	}

	// Update user password
	if err := s.UserService.UpdateUserPassword(user.ID, resetTokenMsg.Password); err != nil {
		log.Println(err.Error())
		http.Error(w, `{"error":"Something went wrong"}`, http.StatusInternalServerError)
		return
	}

	s.UserService.ClearResetToken(user.ID)
	fmt.Fprint(w, `{"success":"Password Reset Successfully"}`)
}

// ResetTokenMessage Represents the post request data of
// password reset token
type ResetTokenMessage struct {
	Password string `json:"password"`
	Token    string `json:"token"`
}

// HandleConsumeOauthCode will handle the Google oauth authorization code
func (s *Server) HandleConsumeOauthCode(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	authCode := query.Get("code")
	clientRedirectURI := query.Get("redirect_uri")

	if authCode == "" {
		errMsg := fmt.Sprintf(`{"error": "%s"}`, "No Authorization Code")
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	// Get Token
	token, err := utils.ExchangeCodeForToken(authCode, clientRedirectURI)
	if err != nil {
		errResp := fmt.Sprintf(`{"error": "%s"}`, err.Error())
		http.Error(w, errResp, http.StatusBadRequest)
		return
	}

	// Get User Details
	googleUser, err := utils.GetGoogleUserInfo(token)
	if err != nil {
		errResp := fmt.Sprintf(`{"error": "%s"}`, err.Error())
		http.Error(w, errResp, http.StatusInternalServerError)
		return
	}

	// Check if user already exists with the same email
	dbUser, err := s.UserService.FindUserByEmail(googleUser.Email)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println(err)
			errResp := fmt.Sprintf(`{"error": "%s"}`, err.Error())
			http.Error(w, errResp, http.StatusInternalServerError)
			return
		}
	}

	if dbUser != nil {
		if dbUser.RegMethod != "google" {
			http.Error(w, fmt.Sprintf(`{"error": "%s"}`, "Password required for sign in"), http.StatusBadRequest)
			return
		}
	} else {
		var user entity.User
		user.Email = googleUser.Email
		user.GoogleID = googleUser.Sub
		user.FullName = googleUser.Name
		err = s.UserService.CreateGoogleUser(user)
		if err != nil {
			log.Println(err)
			errResp := fmt.Sprintf(`{"error": "%s"}`, err.Error())
			http.Error(w, errResp, http.StatusInternalServerError)
			return
		}
	}

	// Get the stored user's ID and email
	dbUser, err = s.UserService.FindUserByEmail(googleUser.Email)
	if err != nil {
		log.Println(err)
		errResp := fmt.Sprintf(`{"error": "%s"}`, err.Error())
		http.Error(w, errResp, http.StatusInternalServerError)
		return
	}

	// Generate JWT
	jwt, err := utils.GenerateJWT(utils.JWTPayload{
		Email: dbUser.Email,
		ID:    dbUser.ID,
	}, s.JWTSecret)
	if err != nil {
		respMsg := fmt.Sprintf(`{"error": "%s"}`, err.Error())
		http.Error(w, respMsg, http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, `{ "status": "success", "token": %q}`, jwt)
}
