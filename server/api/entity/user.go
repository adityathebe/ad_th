package entity

import (
	"time"
)

// User represents a user.
type User struct {
	ID                int       `json:"id"`
	FullName          string    `json:"full_name"`
	Address           string    `json:"address"`
	Telephone         string    `json:"telephone"`
	Email             string    `json:"email"`
	Password          string    `json:"password"`
	GoogleID          string    `json:"google_id"`
	RegMethod         string    `json:"reg_method"`
	ResetToken        string    `json:"reset_token"`
	ResetTokenExpTime time.Time `json:"reset_token_exp_time"`
}

// UserUpdate represents a set of fields that can be updated
type UserUpdate struct {
	FullName  string `json:"full_name"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Email     string `json:"email"`
}

// UserProfile represents a set of fields that are accessible to api users
type UserProfile struct {
	FullName  string `json:"full_name"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Email     string `json:"email"`
}

// UserService represents a service for managing users.
type UserService interface {
	FindUserByID(id int) (*User, error)
	FindUserByEmail(email string) (*User, error)
	CreateUser(user User) error
	CreateGoogleUser(user User) error
	UpdateUser(userID int, user UserUpdate) error
	FindUserByResetToken(token string) (*User, error)
	UpdateUserPassword(userID int, password string) error
	ClearResetToken(userID int) error
	UpdateResetToken(userID int, token string, expTime time.Time) error
}
