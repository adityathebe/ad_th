package api

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/adityathebe/assignment/server/api/entity"
	"github.com/gorilla/mux"
)

// RegisterUserRoutes is a helper function for registering all user routes.
func (s *Server) RegisterUserRoutes(r *mux.Router) {
	r.HandleFunc("/user/profile", s.JWTMiddleware(s.HandleGetUserProfile)).Methods("GET")
	r.HandleFunc("/user/profile", s.JWTMiddleware(s.HandleEditUserProfile)).Methods("PATCH")
}

// HandleGetUserProfile returns the authenticated user's profile
func (s *Server) HandleGetUserProfile(w http.ResponseWriter, r *http.Request) {
	sessionUser := r.Context().Value("user").(*entity.User)
	user, err := s.UserService.FindUserByID(sessionUser.ID)
	if err != nil {
		if err == sql.ErrNoRows {
			respMsg := `{"error": "User Not Found"}`
			http.Error(w, respMsg, http.StatusBadRequest)
			return
		}
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user.Password = ""
	b, err := json.Marshal(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, `{ "error": "%v"}`, err.Error())
		log.Println(err)
		return
	}
	fmt.Fprint(w, string(b))
}

// HandleEditUserProfile updates the authenticated user's profile
func (s *Server) HandleEditUserProfile(w http.ResponseWriter, r *http.Request) {
	sessionUser := r.Context().Value("user").(*entity.User)

	var userUpdate entity.UserUpdate
	if err := json.NewDecoder(r.Body).Decode(&userUpdate); err != nil {
		errMsg := fmt.Sprintf(`{ "error": "%v"}`, err.Error())
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	if err := s.UserService.UpdateUser(sessionUser.ID, userUpdate); err != nil {
		errMsg := fmt.Sprintf(`{ "error": "%v"}`, err.Error())
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, `{"success": "User Profile Edited Succesfully"}`)
}
