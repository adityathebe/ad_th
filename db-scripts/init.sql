CREATE TABLE users (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  full_name VARCHAR(30),
  address VARCHAR(30),
  telephone VARCHAR(30),
  password BINARY(60),
  email VARCHAR(50) UNIQUE,
  google_id VARCHAR(50) UNIQUE,
  reg_method VARCHAR(30) NOT NULL DEFAULT "local",
  reset_token VARCHAR(40),
  reset_token_exp_time timestamp
)