# References

- https://github.com/go-sql-driver/mysql/issues/34#issuecomment-158391340https://medium.com/aubergine-solutions/how-i-handled-null-possible-values-from-database-rows-in-golang-521fb0ee267
- https://github.com/go-sql-driver/mysql/issues/34#issuecomment-158391340
- https://gocodecloud.com/blog/2016/11/15/simple-golang-http-request-context-example/

## Limitations

- No rate limit for password-reset token, login and sign up feature.
- No validation. Example
  * Password Length
  * Email format validity
- Since I wasn't allowed to use external libraries, I had to implement JWT myself. In the intereset of time, I wrote an extremely minimal JWT implementation. Below are some of the limitations of the JWT implementation
  * Same JWT is produced for a user everytime.
  * Doesn't expire.
  * Cannot blacklist tokens.
  * No provision for refresh tokens
  * The JWT implementation supports only HS256 algorithm
- Lack of Context API for cancellation
- Plain authentication for SMTP. I tried setting up yandex SMTP instead of Google to avoid the security alert issue however it didn't work for some reason.
